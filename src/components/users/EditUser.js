import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';

const EditUser = () => {

    let history = useHistory();
    const { id } = useParams();
    // alert(id);

    const [editUsers, setEditUsers] = useState({
        name: "",
        username: "",
        email: "",
        phone: "",
        website: ""
    });

    const { name, username, email, phone, website } = editUsers;

    const onInputChange = e => {
        setEditUsers({ ...editUsers, [e.target.name]: e.target.value })
        console.log(e.target.value);
    }

    useEffect(() => {
        loadUser();
    }, []);

    const onSubmit = async e => {
        e.preventDefault();
        await axios.put(`http://localhost:5000/users/${id}`, editUsers);
        history.push("/");
    }

    const loadUser = async () => {
        const result = await axios.get(`http://localhost:5000/users/${id}`)
        setEditUsers(result.data);
        console.log(result);
    }
    return (
        <div className="container">
            <div className="w-75 mx-auto shadow p-5">
                <h2 className="text-center mb-4">Edit A User</h2>
                <form onSubmit={e => onSubmit(e)}>
                    <div className="form-group">
                        <input
                            type="text"
                            className="form-control form-control-lg"
                            placeholder="Edit Your Name"
                            name="name"
                            value={name}
                            onChange={e => onInputChange(e)}
                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="text"
                            className="form-control form-control-lg"
                            placeholder="Edit Your Username"
                            name="username"
                            value={username}
                            onChange={e => onInputChange(e)}

                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="email"
                            className="form-control form-control-lg"
                            placeholder="Edit Your E-mail Address"
                            name="email"
                            value={email}
                            onChange={e => onInputChange(e)}

                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="text"
                            className="form-control form-control-lg"
                            placeholder="Edit Your Phone Number"
                            name="phone"
                            value={phone}
                            onChange={e => onInputChange(e)}

                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="text"
                            className="form-control form-control-lg"
                            placeholder="Edit Your Website Name"
                            name="website"
                            value={website}
                            onChange={e => onInputChange(e)}

                        />
                    </div>
                    <button className="btn btn-warning btn-block">Update User</button>
                </form>
            </div>
        </div>
    );
};

export default EditUser;