import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';

const ViewUsers = () => {
    const [viewUsers, setviewUsers] = useState({
        name: "",
        username: "",
        email: "",
        phone: "",
        website: ""
    });

    const { id } = useParams();

    useEffect(() => {
        loadUser();
    }, []);

    const loadUser = async () => {
        const res = await axios.get(`http://localhost:5000/users/${id}`);
        setviewUsers(res.data)
    }

    return (
        <div className="container py-4">
            <Link className="btn btn-success" to="/">Back to Home</Link>
            <h1 className="display-4">User Id: {id}</h1>
            <hr />
            <ul className="list-group w-50" >
                <li className="list-group-item">name: {viewUsers.name}</li>
                <li className="list-group-item">user name: {viewUsers.username}</li>
                <li className="list-group-item">email: {viewUsers.email}</li>
                <li className="list-group-item">phone: {viewUsers.phone}</li>
                <li className="list-group-item">website: {viewUsers.website}</li>
            </ul>
        </div>
    );
};

export default ViewUsers;