import React from 'react';

const About = () => {
    return (
        <div className="container">
            <div className="py-4"></div>
            <h2>This is About</h2>
            <p className="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia illo in soluta iusto repellat nesciunt ipsam officia expedita natus ducimus.</p>
        </div>
    );
};

export default About;